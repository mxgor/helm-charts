# Introduction

This repo contains common Helm Charts for XFSC which can be used in the associated projects. E.g. Library Charts.


# Charts

|Name|Purpose|Folder|
|----|-------|------|
|Library| This chart is a library chart which contains helpers for building helm charts.| [Click](/library) |